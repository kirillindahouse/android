package com.example.kirill.colorsofthefuture;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ColorsActivity extends Activity {

    public TextView redValue, greenValue, blueValue;
    public ImageView imageView, colordisp;
    public String redColor, greenColor, blueColor;



    @SuppressLint({"ClickableViewAccessibility", "WrongViewCast"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_colors);

        imageView = findViewById(R.id.artboard);
        colordisp = findViewById(R.id.color_disp);
        redValue = findViewById(R.id.red_rgb);
        greenValue = findViewById(R.id.green_rgb);
        blueValue = findViewById(R.id.blue_rgb);

        imageView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                try{
                    final int evX = (int) event.getX();
                    final int evY = (int) event.getY();

                    int touchColor = getColor(imageView, evX, evY);

                    int r = (touchColor >> 16) & 0xFF;
                    int g = (touchColor >> 8) & 0xFF;
                    int b = (touchColor >> 0) & 0xFF;

                    redColor = String.valueOf(r);
                    greenColor = String.valueOf(g);
                    blueColor = String.valueOf(b);

                    redValue.setText(redColor);
                    greenValue.setText(greenColor);
                    blueValue.setText(blueColor);

                    colordisp.setBackgroundColor(touchColor);

                }catch (Exception e){}
                return true;
            }
        });
    }
    private int getColor(ImageView imageView, int evX, int evY){
        imageView.setDrawingCacheEnabled(true);
        Bitmap bitmap = Bitmap.createBitmap(imageView.getDrawingCache());
        imageView.setDrawingCacheEnabled(false);
        return bitmap.getPixel(evX, evY);
    }
}